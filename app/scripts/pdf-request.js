;(function ($, api, loader, viewPdf, urlGenerator, timeout, exports) {
  'use strict';

  var Requester = function ($form) {
    this.$form = $form;
    this.$submitButton = $form.find('button[type="submit"]');
    this.$urlInput = $form.find('input[type="text"]');
    this.$error = $form.find('.error');
  };

  Requester.prototype.displayPdf = function (hash) {
    var pdfUrl = api.retrievePdfUrl(hash);

    timeout(function () {
      loader.stop();
      viewPdf(pdfUrl);
    }, 2000);
  };

  Requester.prototype.check = function (hash) {
    var self = this;
    api
      .check(hash)
      .done(function (data, b, c) { self.displayPdf(hash); })
      .fail(function (xhr) { self.failure(xhr); })
    ;
  };

  Requester.prototype.failure = function (xhr) {
    loader.stop();
    this.$error.text('Youps ... it appears that your pdf is not correct :-(');
  };

  Requester.prototype.generate = function () {
    var self = this;
    var url = urlGenerator(this.$urlInput.val());

    if (!url.protocol()) {
      url.protocol('http');
    }

    url = url.toString();

    api
      .generate(url)
      .done(function (data) {
        self.check(data['hash']);
      })
      .fail(function (xhr) {
        self.failure(xhr);
      })
    ;
  };

  Requester.prototype.initialize = function () {
    var self = this;
    this.$form.submit(function (event) {
      event.preventDefault();
      loader.start(self.$submitButton);
      self.generate();
    });
  };

  exports.pdfRequest = function ($form) {
    var requester = new Requester($form);
    requester.initialize();
  };
})(jQuery, window.api, window.spinner, window.viewPdf, window.URI, window.setTimeout, window);
