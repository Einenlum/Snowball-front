;(function (Spinner, exports) {
  'use strict';

  var $loadingBlock;
  var $oldBlock;
  var running = false;
  var spinner;
  var options = {
    lines: 9, // The number of lines to draw
    length: 16, // The length of each line
    width: 5, // The line thickness
    radius: 12, // The radius of the inner circle
    corners: 0, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#000', // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: true, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: '50%', // Top position relative to parent
    left: '50%' // Left position relative to parent
  };

  var startSpin = function (element) {
    if (running) {
      throw new Error('The spinner is already running');
    }

    $loadingBlock = $('<div class="spinner-container"></div>');
    $oldBlock = element;

    $oldBlock.after($loadingBlock);
    $oldBlock.remove();

    spinner = new Spinner(options);
    spinner.spin($loadingBlock[0]);
    running = true;
  };

  var stopSpin = function () {
    if (!running) {
      throw new Error('The spinner is not running :-(');
    }

    spinner.stop();

    $loadingBlock.after($oldBlock);
    $loadingBlock.remove();

    running = false;
  };

  exports.spinner = {
    start: startSpin,
    stop: stopSpin
  };
})(window.Spinner, window);
