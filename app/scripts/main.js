(function ($, snow, pdf) {
  'use strict';

  snow();
  var main = function () {
    pdf($('#submit-form'));
    snow();

    $('div#page-wrapper').slideScroll();

    $('a[data-role="anchor-link"]').click(function (event) {
      event.preventDefault();
      var href = $(this).attr('href');
      $('div#page-wrapper').slideScroll(href);
      currentLocation.href = href;
    });
  };

  $(main);
})(jQuery, window.snow, window.pdfRequest);
