;(function (exports, PdfJS, $, uri, location) {
  'use strict';

  var viewPdf = function (fileUrl) {
    var popup = new $.Popup({
      width: $(window).width() - 100,
      height: $(window).height() - 100,
    });
    var url = uri(location.href);
    url
      .resource('/scripts/vendor/pdfjs/web/viewer.html')
      .query({'file': fileUrl})
    ;

    popup.open(url.toString());
  };

  exports.viewPdf = viewPdf;
})(window, window.PDFJS, window.jQuery, window.URI, window.location);

