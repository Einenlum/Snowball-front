;(function api($, config, uriGen, exports) {
  'use strict';

  var query = function (uri, data, type, dataType) {
    var url = uriGen(config.apiDomain);
    if ('/' !== url.resource()) {
      url = url.resource(url.resource() + '/' + uri);
    } else {
      url = url.resource(uri);
    }
    url = url.query(data).toString();

    type = undefined === type ? 'GET' : type;
    dataType = undefined === dataType ? 'json' : dataType;

    console.log(url);

    return $.ajax({
      url: url,
      async: true,
      crossDomain: true,
      type: type,
      dataType: dataType
    });
  };

  var api = {
    generate: function(url) {
      return query('generate', {'url': url});
    },
    check: function(hash) {
      return query('check', {'hash': hash});
    },
    retrievePdfUrl: function (hash) {
      var url = uriGen(config.apiDomain);
      if ('/' !== url.resource()) {
        url = url.resource(url.resource() + '/file');
      } else {
        url = url.resource('/get');
      }

      return url.query({'hash': hash}).toString();
    }
  };

  exports.api = api;
})(jQuery, window.config, window.URI, window);

