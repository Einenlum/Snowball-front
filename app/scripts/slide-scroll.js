;(function (exports, $, UriJS, url) {
  var initializeSlideScroll = function($wrapper, href) {
    if (href) {
      var page = href.substr(1);
      var firstPage = false;
    } else {
      var hash = UriJS(url.hash);
      var page = hash.fragment();
      var firstPage = true;
    }

    if (firstPage) {
      $wrapper.find('div.slide-item').hide();
    } else {
      $wrapper.find('div.slide-item').fadeOut();
    }
    if (page === 'login') {
      var pageToLoad = 'div#login';
    } else if (page === 'register') {
      var pageToLoad = 'div#register';
    } else {
      var pageToLoad = 'div#home';
    }

    if (firstPage) {
      $wrapper.find(pageToLoad).show();
    } else {
      $wrapper.find(pageToLoad).delay(400).fadeIn();
    }
  };

  exports.slideScroll = function (href) {
    $wrapper = $(this);
    initializeSlideScroll($wrapper, href);
  };
})(window.jQuery.fn, jQuery, window.URI, window.location.href);

